package com.trendyolbootcamp.linkedinclonecandidate.service;

import com.trendyolbootcamp.linkedinclonecandidate.domain.Candidate;
import com.trendyolbootcamp.linkedinclonecandidate.repository.CandidateRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CandidateServiceTests {

    @Mock
    CandidateRepository candidateRepository;

    @InjectMocks
    CandidateService candidateService;

    @Test
    public void it_should_get_candidate_when_call_getCandidateById() {
        Candidate candidate = new Candidate("Ahmet");

        Mockito.when(candidateRepository.findByIdOptional(candidate.getId()))
                .thenReturn(Optional.of(candidate));

        Candidate result = candidateService.getCandidateByID(candidate.getId());

        assertEquals("Ahmet", result.getName());
    }

    @Test
    public void it_should_delete_candidate_when_call_deleteCompany(){
        Candidate candidate = new Candidate("Test");
        Mockito.when(candidateRepository.insert(candidate)).thenReturn(candidate);

        candidateService.deleteCandidate(candidate.getId());

        assertEquals(true, candidateService.findAll().isEmpty());
    }

}
